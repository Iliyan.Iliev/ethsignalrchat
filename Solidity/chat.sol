pragma solidity >=0.4.22 <0.6.0;
contract Chat {

    mapping(string => Message) messages;

    struct Message {
        string message;
        string sender;
        uint timestamp;
    }

    function storeMessage(string memory _msg, string memory sender, string memory id) public returns (bool) {
        require(bytes(_msg).length > 0);
        Message memory message = Message(_msg, sender, block.timestamp);
        messages[id] = message;
    }
    
    function getMessage(string memory id) public view returns (string memory){
        return messages[id].message;
    }
    
    
}
