﻿using System.Threading.Tasks;
using Nethereum.Hex.HexTypes;
using Nethereum.Signer;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using SignalR.Interfaces;

namespace SignalRSimpleChat
{
    public class BlockchainClient : IBlockchainClient
    {
        private const string ScAddress = "0xcf93b0c213b830ac18810dfed4fdc997cd64e390";
        private const string PrivateKey = "9392ECDEA8F2B45516BD180DF6A3FC6A3DB3C5123100311A12AABFF721031170";
        private const string Abi = "[\r\n\t{\r\n\t\t\"constant\": false,\r\n\t\t\"inputs\": [\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"string\",\r\n\t\t\t\t\"name\": \"_msg\",\r\n\t\t\t\t\"type\": \"string\"\r\n\t\t\t},\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"string\",\r\n\t\t\t\t\"name\": \"sender\",\r\n\t\t\t\t\"type\": \"string\"\r\n\t\t\t},\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"string\",\r\n\t\t\t\t\"name\": \"id\",\r\n\t\t\t\t\"type\": \"string\"\r\n\t\t\t}\r\n\t\t],\r\n\t\t\"name\": \"storeMessage\",\r\n\t\t\"outputs\": [\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"bool\",\r\n\t\t\t\t\"name\": \"\",\r\n\t\t\t\t\"type\": \"bool\"\r\n\t\t\t}\r\n\t\t],\r\n\t\t\"payable\": false,\r\n\t\t\"stateMutability\": \"nonpayable\",\r\n\t\t\"type\": \"function\"\r\n\t},\r\n\t{\r\n\t\t\"constant\": true,\r\n\t\t\"inputs\": [\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"string\",\r\n\t\t\t\t\"name\": \"id\",\r\n\t\t\t\t\"type\": \"string\"\r\n\t\t\t}\r\n\t\t],\r\n\t\t\"name\": \"getMessage\",\r\n\t\t\"outputs\": [\r\n\t\t\t{\r\n\t\t\t\t\"internalType\": \"string\",\r\n\t\t\t\t\"name\": \"\",\r\n\t\t\t\t\"type\": \"string\"\r\n\t\t\t}\r\n\t\t],\r\n\t\t\"payable\": false,\r\n\t\t\"stateMutability\": \"view\",\r\n\t\t\"type\": \"function\"\r\n\t}\r\n]";
        private const string StoreMessageFuncName = "storeMessage";
        private const string GetMessageFuncName = "getMessage";
        private const string PublicKey = "0xed49ceeda548fe51bf87f7f838b0e1a5270d5981";
        private const string NodeUrl = "https://ropsten.infura.io/v3/d833d12085904886b6637785537de6ae";

        private readonly MessageSigner _messageSigner;
        private readonly Web3 _web3;

        public BlockchainClient()
        {
            _messageSigner = new MessageSigner();
            _web3 = new Web3(new Account(PrivateKey), NodeUrl);
        }

        public async Task StoreMessageInBlockchain(string sender, string message, string id)
        {
            var contract = _web3.Eth.GetContract(Abi, ScAddress);
            var storeMessageFunc = contract.GetFunction(StoreMessageFuncName);
            var parameters = new object[] { message, sender, id };

            await storeMessageFunc.SendTransactionAsync(
                PublicKey,
                gas: new HexBigInteger(1000000),
                new HexBigInteger(100000000000),
                new HexBigInteger(0), parameters);
        }

        public Task<string> GetMessageFromBlockchain(string id)
        {
            var contract = _web3.Eth.GetContract(Abi, ScAddress);
            var getMessageFunc = contract.GetFunction(GetMessageFuncName);

            return getMessageFunc.CallAsync<string>(id);
        }

        public string HashAndSign(string message)
        {
            var signature = _messageSigner.HashAndSign(message, PrivateKey);
            return signature;
        }

        public bool VerifyMessage(string plainTextMessage, string signedMessage)
        {
            var signature = HashAndSign(plainTextMessage);
            return signature == signedMessage;
        }
    }
}