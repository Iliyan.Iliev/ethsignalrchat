﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SignalR.Interfaces;
using SignalRSimpleChat;

namespace SignalR.Services
{
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly IBlockchainClient _blockchainClient;

        public ChatHub(IBlockchainClient blockchainClient)
        {
            _blockchainClient = blockchainClient;
        }

        public async Task SendMessage(string message)
        {
            var id = Guid.NewGuid().ToString();
            var username = Context.GetHttpContext().User.Identity.Name;
            await Clients.All.SendAsync("DisplayMessage",id, username, message);
            message = _blockchainClient.HashAndSign(message);
            await _blockchainClient.StoreMessageInBlockchain(username, message, id);
        }

        public override async Task OnConnectedAsync()
        {
            var username = Context.GetHttpContext().User.Identity.Name;
            await Clients.Others.SendAsync("DisplayNewUser", username);
        }
    }
}