﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SignalR.Data.Entities
{
    public class MessageEntity
    {
        [Key]
        public long Id { get; set; }
        public string Username { get; set; }
        public string ReceiverId { get; set; }
        public string MessageText { get; set; }
        public DateTime Timestamp { get; set; }
    }
}