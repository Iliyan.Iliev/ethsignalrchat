﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SignalR.Interfaces;
using SignalR.Models;
using SignalRSimpleChat;

namespace SignalR.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBlockchainClient _blockchainClient;

        public HomeController(IBlockchainClient blockchainClient)
        {
            _blockchainClient = blockchainClient;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AuditMessage(string id, string message)
        {
            var messageFromBlockchain = await _blockchainClient.GetMessageFromBlockchain(id);

            if (string.IsNullOrEmpty(messageFromBlockchain))
                return NotFound();

            var result = _blockchainClient.VerifyMessage(message, messageFromBlockchain);

            return Ok(result);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
