﻿using System;

namespace SignalR.Models
{
    public class Message
    {
        public string Author { get; set; }
        public string MessageText { get; set; }
        public DateTime Timestamp { get; set; }
    }
}