﻿using System.Threading.Tasks;

namespace SignalR.Interfaces
{
    public interface IBlockchainClient
    {
        Task StoreMessageInBlockchain(string sender, string message, string id);

        Task<string> GetMessageFromBlockchain(string id);

        string HashAndSign(string message);

        bool VerifyMessage(string plainTextMessage, string signedMessage);
    }
}